# Releases 

# Warning:  
DO NOT USE WITH SD2Vita/PSVSD (on ux0) IF YOU DO NOT HAVE ENSO!  
it WILL ask you to format your memory card upon reboot!  
if you MUST switch accounts. you can manually remove /id.dat from the REAL memory card/internal storage.  


# v1.2
+ Now unlinks the account completely (sets account_id to 0x00)
+ Now re-enables trophy eligibility for itself after doing so.

VPK: https://bitbucket.org/SilicaAndPina/simpleaccountswitcher/downloads/SimpleAccountSwitcher-1.2.vpk

# v1.1
+ Fixed the bug where the sign up app would return to the SimpleAccountSwitcher thus resetting account again
+ Improved the code (thx celeste!)

VPK: https://bitbucket.org/SilicaAndPina/simpleaccountswitcher/downloads/SimpleAccountSwitcher-1.1.vpk  

# v1.0

A Simple app to clear out your PSN login details and run the Sign Up application, allowing you to switch PSN accounts.

VPK: https://bitbucket.org/SilicaAndPina/simpleaccountswitcher/downloads/SimpleAccountSwitcher-1.0.vpk  
