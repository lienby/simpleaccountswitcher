#include <psp2/power.h>
#include <psp2/io/fcntl.h>
#include <psp2/kernel/processmgr.h>
#include <psp2/kernel/threadmgr.h>
#include <psp2/appmgr.h>
#include <psp2/types.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int sceRegMgrSetKeyInt(const char*, const char*, int);
int sceRegMgrSetKeyStr(const char*, const char*, const char*, int);
int sceRegMgrGetKeyBin(const char *category, const char *name, void *buf, int size);

int executeUriExit(char *uritext) {
	while(1) {
		sceAppMgrLaunchAppByUri(0x20000, uritext);
	}
}

int main(int argc, const char *argv[]) {
        //setup buffer shit because C sucks.
	char key_buf[8];
	uint64_t uni_key = 0x0000000000000000LL;
	memset(key_buf, 0, sizeof(key_buf));
	memcpy(&key_buf[0], &uni_key, sizeof(key_buf));
        
        //set keys.
	sceRegMgrSetKeyStr("/CONFIG/NP", "login_id", (char[]){0x0}, 1);
	sceRegMgrSetKeyStr("/CONFIG/NP", "password", (char[]){0x0}, 1);
        sceRegMgrSetKeyStr("/CONFIG/NP", "lang", (char[]){0x0}, 1);
        sceRegMgrSetKeyStr("/CONFIG/NP", "country", (char[]){0x0}, 1);
        sceRegMgrSetKeyInt("/CONFIG/NP", "yob", 0);
        sceRegMgrSetKeyInt("/CONFIG/NP", "mob", 0);
        sceRegMgrSetKeyInt("/CONFIG/NP", "dob", 0);
        sceRegMgrSetKeyInt("/CONFIG/NP", "has_subaccount", 0);
        sceRegMgrSetKeyInt("/CONFIG/NP", "enable_np", 0);
        sceRegMgrSetKeyBin("/CONFIG/NP", "account_id", key_buf, sizeof(key_buf));
        
        //remove id.dat
	sceIoRemove("ux0:/id.dat");
        
        //remove the savedata created at boot:
        sceIoRemove("savedata0:/sce_sys/param.sfo");
        sceIoRemove("savedata0:/sce_sys/sealedkey");
        sceIoRemove("savedata0:/sce_sys/keystone");
        sceIoRemove("savedata0:/sce_sys/_safemem.dat");
        sceIoRemove("savedata0:/sce_sys/sdslot.dat");
        
        //run psnReg
	executeUriExit("psnreg:");
	sceKernelExitProcess(0);
	return 0;
}